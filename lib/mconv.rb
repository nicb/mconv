#
# $Id: mconv.rb 1447 2011-05-08 15:06:51Z nicb $
#
$:.unshift(File.dirname(__FILE__)) unless
  $:.include?(File.dirname(__FILE__)) || $:.include?(File.expand_path(File.dirname(__FILE__)))

module Mconv

  EXT_LIB_PATH = 'extensions' unless defined?(EXT_LIB_PATH)
  MCONV_LIB_PATH = 'mconv' unless defined?(MCONV_LIB_PATH)

  %w(
    integer
    string
  ).each { |f| require File.join(EXT_LIB_PATH, f) }

  %w(
    version
    parser
    cli
  ).each { |f| require File.join(MCONV_LIB_PATH, f) }

end
