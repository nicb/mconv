#
# $Id: globals.rb 1447 2011-05-08 15:06:51Z nicb $
#

require 'singleton'

module Mconv

  class Globals

    include Singleton

    attr_accessor :tempo, :cur_time, :line_number, :cur_file
    attr_reader   :note_stack, :chord_stack, :parse_stack, :tuplet_register

    def initialize
      @tempo = 120  # default M.M. marking
      @note_stack = {}
      @chord_stack = []
      @parse_stack = []
      @tuplet_register = nil
      @cur_time = 0.0
      @line_number = 0
    end

    def tempo_in_secs
      (60.0 / self.tempo)
    end

    def bump_line_number(cnt = 1)
      self.line_number += cnt
    end

  end

end
