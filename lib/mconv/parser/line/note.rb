#
# $Id: note.rb 1447 2011-05-08 15:06:51Z nicb $
#

module Mconv

  module Parser

    module Line

      class NoteNameError < StandardError; end

      class Note < TimeAnchored

        attr_accessor :note, :dur, :notes

        def initialize(line)
          ee = super(line)
          (note, durnum, durden, notes) = ee.split(/:/)
          self.note = note
          self.dur = Time.new(durnum, durden)
          self.notes = notes
        end

        def to_ly
          lynote = note_name_to_ly
        end

      private
      
        def note_name_to_ly
          oidx = self.note.index(/[\+\-]?\d/)
          raise NoteNameError, "wrong note name #{self.note}" unless oidx
          (nominal_full_name, octave) = self.note[0..oidx-1], self.note[oidx..-1]
          raise NoteNameError, "wrong note name #{self.note}" unless nominal_full_name && octave
          (nominal_note_name, accidents) = [nominal_full_name[0], nominal_full_name[1..-1]]
          raise NoteNameError, "wrong note name #{self.note}" unless nominal_note_name && nominal_note_name =~ /[a-g]/
          ly_accidents = accidents_to_ly(accidents)
          raise NoteNameError, "wrong note name #{self.note}" unless nominal_note_name
          ly_octave = octave_to_ly(octave)
          result = nominal_note_name + ly_accidents + ly_octave
          return result
        end
      
        def accidents_to_ly(orig)
          raise NoteNameError, "wrong accidentals for #{self.note}" unless orig.characters_all_the_same?
          return orig[0] =~ /#/ ? orig.gsub(/#/, 'is') : orig.gsub('f', 'es') if orig
        end
        
        def octave_to_ly(orig)
          result = ''
          orig = orig.to_i
          offset = 3
          sign = ''
          n = 0
          if orig > offset
            sign = "'"
            n = orig - offset
          elsif orig < offset
            sign = ','
            n = offset - orig
          end
          1.upto(n) { result += sign }
                return result
        end

      end
      
    end

  end

end
