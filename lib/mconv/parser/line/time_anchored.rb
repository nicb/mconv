#
# $Id: time_anchored.rb 1447 2011-05-08 15:06:51Z nicb $
#

module Mconv

  module Parser

    module Line

	    class TimeAnchored < Base
	
	      attr_accessor :at_secs, :at
	
	      def initialize(line)
	        (atnum, atden, everything_else) = line.split(/:/, 3)
		(atnum, atden) = atnum.to_f, atden.to_f
                self.at = Time.new(atnum, atden)
                self.at_secs = calc_time(atnum, atden)
                everything_else
	      end
	
	    protected
	
	      def calc_time(num, den)
                tmetro = Mconv::Globals.instance.tempo_in_secs
                (tmetro/den) * num
	      end
	
	    end

    end

  end

end
