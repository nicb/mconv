#
# $Id: time.rb 1447 2011-05-08 15:06:51Z nicb $
#

module Mconv

  module Parser

    module Line

	    class Time

        attr_reader :num, :den

        def initialize(n, d)
          @num = n
          @den = d
        end

        def tuplet?
          !self.den.power_of_two?
        end

	    end

    end

  end

end
