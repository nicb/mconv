#
# $Id: tuplet.rb 1447 2011-05-08 15:06:51Z nicb $
#

module Mconv

  module Parser

    module Line

	    class Tuplet < TimeAnchored

        attr_reader :elements
        attr_accessor :signature_num, :signature_den

        def initialize
          @elements = []
        end

        def <<(t)
          raise(ArgumentError, "object #{t.inspect} is not a class Mconv::Parser::Line::Time object" unless t.is_a?(Mconv::Parser::Line::Time)
          if self.elements.empty?
            self.signature_num = t.den.nearest_lower_power_of_two
            self.signature_den = t.den
          end
          self.elements << t
        end

        def to_s
          # TODO
          # "\\times %d/%d { %s }" % []
        end

	    end

    end

  end

end
