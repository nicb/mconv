#
# $Id: line.rb 1447 2011-05-08 15:06:51Z nicb $
#

module Mconv

  module Parser

    LINE_PARSE_PATH = File.join(File.dirname(__FILE__), 'line') unless defined?(LINE_PARSE_PATH)

    %w(
      error
      base
      time_anchored
      time
      comment
      bar_ending
      tempo
      ratio
      note
    ).each { |f| require File.join(LINE_PARSE_PATH, f) }

  end

end
