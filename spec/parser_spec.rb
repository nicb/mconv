# frozen_string_literal: false # it must be so to pass exception expectation

RSpec.describe Mconv::Parser do

  before do
    @mat = Dir.glob(File.join(['..'] * 2, 'sections', 's??.dat')).sort
    @wrong_line = "this ilne is wrong"
  end

  it 'scans files properly' do
    @mat.each do
      |sect|
      File.open(sect, 'r') do
        |f|
        Mconv::Globals.instance.cur_file = f.path
        cnt = 0
        while(line = f.gets)
          cnt += 1
          Mconv::Parser::Line::Base.scan(line, cnt)
        end
        expect(cnt > 0).to(be(true), "#{cnt}")
        expect(Mconv::Globals.instance.parse_stack.empty?).not_to(be(true))
      end
    end
  end

  it 'catches errors while scanning' do
    expect { Mconv::Parser::Line::Base.scan(@wrong_line, 1) }.to raise_error(Mconv::Parser::Line::Base::ParseError)
  end

end
