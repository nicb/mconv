# frozen_string_literal: true

RSpec.describe Mconv::CLI do

  before do
    @stdout_io = StringIO.new
  end

  it 'prints some output' do
    Mconv::CLI.execute(@stdout_io, [])
    @stdout_io.rewind
    expect(@stdout_io.read).to match(/To update this executable/)
  end

end
